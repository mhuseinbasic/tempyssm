awk '/deb \[signed-by=/{
      pubkey = $2;
      sub(/\[signed-by=/, "", pubkey);
      sub(/\]$/, "", pubkey);
      print pubkey
    }' /etc/apt/sources.list.d/gitlab_gitlab-?e.list | \
  while read line; do
    curl -s "https://packages.gitlab.com/gpg.key" | gpg --dearmor > $line
  done
